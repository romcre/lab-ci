#include "integ.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void test_sum_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 0), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(6, integer_avg(10, 2), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_avg(1, 1), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-5, integer_avg(-6,-4), "Error in integer_avg");
}
